/*
 * Copyright (C) 2016 andrio <andrio@andrio-V5-171>
 * 
 */

#include <array>

#include <boost/python.hpp>
#include "boost/filesystem.hpp"
#include "func.h"

namespace bp = boost::python;
namespace bf = boost::filesystem;

bp::list list_frame(const char* filename, double fps, double vuser) {
    cv::VideoCapture video = myopen(filename);
    fps = sfps(video, fps);
    forward_list<double> lframe = listframe(video, fps, vuser);
    video.release();
    bp::list plframe;
    for (double i: lframe) plframe.append(i);
    return plframe;
}

void temp_write(cv::VideoCapture video, forward_list<double> lframe, string filejpg) {
	write(video, lframe, filejpg);
	video.release();
}

long long wframe(bp::list& plframe, const char* filename, 
                 const char* jpgname) {
	string filejpg = jpgname;
	forward_list<double> lframe;
	cv::VideoCapture video = myopen(filename);
	for(int i = 0; i < len(plframe);i++) lframe.push_front(bp::extract<double>(plframe[i]));
	lframe.reverse();
	boost::thread *ptrproc = new boost::thread(&temp_write, video, lframe, filejpg);
	return (long long)ptrproc;
}

void join(long long intproc) {
    boost::thread *ptrproc = ((boost::thread*)intproc);
    ptrproc->join();
    delete ptrproc;
}

void nvideo(const char *videoname, bp::list& plname, const char *imgbase) {
	forward_list<string> lname;
	for(int i = 0; i < len(plname);i++) {
		lname.push_front(bp::extract<string>(plname[i]));
	}
	lname.reverse();
	new_video(videoname, lname, imgbase);
}

//Image operation

class CVImage {
private:
    cv::Mat img;
	vector<int> save_params = {CV_IMWRITE_JPEG_QUALITY, 100};
    
public:
    CVImage(const char* imgname) {
        this->img = cv::imread(imgname);
        if (this->img.empty())
		    throw ("Image is not open");
    }
   
    CVImage(CVImage *nimg) {
        this->img = nimg->img.clone();
        if (this->img.empty())
		    throw ("Image is not open");
    }

    void setRoi(float x, float y, float width, float height) {
		cv::Rect r(x, y, width, height);
        this->img = this->img(r);
    }

    void save(const char* imgname) {
	    bool rwrite = cv::imwrite(imgname, this->img, this->save_params);
        if (!rwrite)
            throw ("Image not save");
    }

    ~CVImage() {
		this->img.release();
	}
};

long long img_open(const char* imgname) {
	CVImage *ptrimg = new CVImage(imgname);
	return (long long)ptrimg;
}

void img_setRoi(long long intimg, float x, float y, float width, float height) {
	CVImage *ptrimg = ((CVImage*)intimg);
    ptrimg->setRoi(x, y, width, height);
}

long long img_copy(long long intimg) {
	CVImage *ptrimg = ((CVImage*)intimg);
	CVImage *new_ptrimg = new CVImage(ptrimg);
	return (long long)new_ptrimg;
}

void img_save(long long intimg, const char* imgname) {
	CVImage *ptrimg = ((CVImage*)intimg);
	ptrimg->save(imgname);
}
	
void img_release(long long intimg) {
	CVImage *ptrimg = ((CVImage*)intimg);
	delete ptrimg;
}

//filesystem operation

void temp_rename(forward_list<array<string, 2>> ltname, bool move) {
    void (*my_rename)(const bf::path&, const bf::path&) = bf::rename;
	if (!move)
	    my_rename = bf::copy_file;
	
	auto it = ltname.begin();
	cycle: {
        try {
            for(;it != ltname.end(); ++it) {
		        array<string, 2> temp = *it;
		        my_rename(bf::path(temp[0]), bf::path(temp[1]));
		    }
		} catch (const boost::filesystem::filesystem_error& e) {
            cerr << "Exc: " << e.what() << endl;
            it++;
            goto cycle;
        }
    }
}

long long lrename(bp::list& pltname, bool move) {
	forward_list<array<string, 2>> ltname;
	for(int i = 0; i < len(pltname);i++) {
		array<string, 2> temp;
		temp[0] = bp::extract<string>(pltname[i][0]);
		temp[1] = bp::extract<string>(pltname[i][1]);
		ltname.push_front(temp);
	}
	boost::thread *ptrproc = new boost::thread(&temp_rename, ltname,
	    move);
	return (long long)ptrproc;
}

void char_translate(const char* reason) {
    PyErr_SetString(PyExc_RuntimeError, reason);
}

void cv_translate(cv::Exception e) {
    PyErr_SetString(PyExc_RuntimeError, e.what());
}

BOOST_PYTHON_MODULE(pycv) {
    bp::register_exception_translator<const char* >(&char_translate);
    bp::register_exception_translator<cv::Exception>(&cv_translate);    
	
    bp::def("list_frame", list_frame);
    bp::def("wframe", wframe);
    bp::def("join", join);
    bp::def("new_video", nvideo);
    bp::def("img_open", img_open);
    bp::def("img_setRoi", img_setRoi);
    bp::def("img_copy", img_copy);
    bp::def("img_save", img_save);
    bp::def("img_release", img_release);
    bp::def("rename", lrename);
}
