/*
 * Copyright (C) 2016 andrio <andrio@andrio-V5-171>
 * 
 */

#include <math.h>
#include <boost/format.hpp>
#include "func.h"

cv::VideoCapture myopen(const char* filename) {
    cv::VideoCapture video(filename);
    if(!video.isOpened()) {
        cout <<"This video is not open: "<< filename <<endl;
        video.release();
        throw ("Video is not open");
    }
    return video;
}

bool is_dublicate(cv::Mat &temp, cv::Mat &frame, cv::Mat &diff, double vuser) {
    cv::compare(temp, frame, diff, cv::CMP_EQ);
    cv::cvtColor(diff, diff, CV_BGR2GRAY);
    if (((double)cv::countNonZero(diff)/(diff.rows * diff.cols)*100) < vuser)         
        return true;
    else
        return false;
}

void myset(cv::VideoCapture &video, cv::Mat &frame, double &i, double &j, bool &rframe) {
	while((i+1) < j) {
	    i++;
	    //cout <<"To set. I= "<< i << " J= " << j << endl;
		rframe = video.read(frame);
        if (!rframe) {
            cout << "Failed create frame in: " << i << endl;
            break;
        } 
    }
}

forward_list<double> listframe(cv::VideoCapture &video, double &fps, double &vuser) {
    forward_list<double> lframe;
    double cframe = video.get(CV_CAP_PROP_FRAME_COUNT);
    cv::Mat frame, temp, diff;
    bool rframe;
    double i = 0;
    for (double j = 0;j < cframe;j += fps) {
		myset(video, frame, i, j, rframe);
        rframe = video.read(frame);
        if (!rframe) {
            cout << "Failed create frame in: " << j << endl;
            break;
        }
        if (j != 0) {
			if (is_dublicate(temp, frame, diff, vuser)) continue;
            lframe.push_front(j);
		}
        temp = frame.clone(); 
    }
    frame.release();
    temp.release();
    diff.release();
    lframe.reverse();
    return lframe;
}

void write(cv::VideoCapture &video, forward_list<double> &lframe, string &filejpg) {
	vector<int> compression_params;
    compression_params.push_back(CV_IMWRITE_JPEG_QUALITY);
    compression_params.push_back(100);
    cv::Mat frame;
    bool rframe, rwrite;
    double i = 0;
    for (double &j: lframe) {
		myset(video, frame, i, j, rframe);
		//cout <<"I= "<< i << " J= " << j << endl;
		rframe = video.read(frame);
		if (!rframe) {
            cout << "Failed create frame in: " << j << endl;
            break;
        }
        rwrite = cv::imwrite((boost::format("%1%_N%2%.jpg") % filejpg % j).str(), frame, compression_params);
        if (!rwrite)
        {
            cout << "Failed save the image in: " << j << endl;
        }
        
    }
    frame.release();
}

void new_video(const char *videoname, forward_list<string> &lname, const char *imgbase) {
    cv::Mat img = cv::imread(imgbase);
    if (img.empty()) {
		cout  << "This image is not open: " << imgbase << endl;
		img.release();
		throw ("Image is not open");
	}

	cv::VideoWriter video;
	video.open(videoname, CV_FOURCC('D','I','V','X'), 10, cv::Size(img.size().width, img.size().height), true);
	if (!video.isOpened())
    {
        cout  << "This video is not open: " << videoname << endl;
        img.release();
        video.release();
        throw ("Video is not open");
    }
	for (string &i: lname) {
		img = cv::imread(i);
		video.write(img);
	    cout << i << endl;
	}
	video.release();
	img.release();
}
