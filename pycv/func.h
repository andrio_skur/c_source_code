/*
 * Copyright (C) 2016 andrio <andrio@andrio-V5-171>
 * 
 */

#include <string>
#include <forward_list>
#include "opencv2/opencv.hpp"

#include <boost/thread.hpp>
#include <iostream>

using namespace std;

cv::VideoCapture myopen(const char* filename);
bool is_dublicate(cv::Mat &temp, cv::Mat &frame, cv::Mat &diff, double vuser);
forward_list<double> listframe(cv::VideoCapture &video, double &fps, double &vuser);
void write(cv::VideoCapture &video, forward_list<double> &lframe, string &filejpg);
void new_video(const char *videoname, forward_list<string> &lname, const char *imgbase);
inline double sfps(cv::VideoCapture &video, double &fps) {
    //if (video.get(CV_CAP_PROP_FPS) < fps) {
    //    cout<<"User fps > fps video. Fps = 1"<<endl;
    //    fps = 1;
    //}
    return fps;
}

