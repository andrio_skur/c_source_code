import cv2
bgs = cv2.createBackgroundSubtractorMOG2()
capture = cv2.VideoCapture(input('src='))
cv2.namedWindow("Original",1)
cv2.namedWindow("Foreground",1)
while True:
            img = capture.read()[1]
            cv2.imshow("Original",img)
            fgmask = bgs.apply(img)
            foreground = cv2.bitwise_and(img,img,mask=fgmask)
            cv2.imshow("Foreground",foreground)
            cv2.waitKey(5)
cv2.destroyAllWindows()
capture.release()
