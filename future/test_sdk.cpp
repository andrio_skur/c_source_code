#include <cmath>
#include <iostream>

#include <boost/format.hpp>
#include "opencv2/opencv.hpp"

//author = 'Andrio'
using namespace std;
using namespace cv;

VideoWriter video;
Mat back, plate1, plate2;

int my_main(int argc, char* argv[]) {
    back = imread(argv[1]);
    if (back.empty()) 
        throw((boost::format("Background is not open= %1%") % argv[1]).str());
    plate1 = imread(argv[2]);
    if (plate1.empty())
        throw((boost::format("One plate is not open= %1%") % argv[2]).str());
    plate2 = imread(argv[3]);
    if (plate2.empty()) 
        throw((boost::format("Two plate is not open= %1%") % argv[3]).str()); 
    std::cout << "Background: " << argv[1] << std::endl;
    std::cout << "One plate: " << argv[2] << std::endl;
    std::cout << "Two plate: " << argv[3] << std::endl;

    double b_width, b_height, p1_width, p1_height, p2_width, p2_height;
    b_width = back.size().width;
    b_height = back.size().height;
    p1_width = plate1.size().width;
    p1_height = plate1.size().height;
    p2_width = plate2.size().width;
    p2_height = plate2.size().height;
    double k = 1;
    double w1, h1, w2, h2, max_w1, max_h1, max_w2, max_h2; 
    w2 = 0;
    h2 = p2_height + k;
    w1 = 200 + p1_width; //distance
    h1 = 790;
    max_w1 = p1_width + k;
    max_h1 = p1_height + k;
    max_w2 = b_width - p2_width - k;
    max_h2 = b_height - p2_height - k;

    video.open(argv[4], CV_FOURCC('D','I','V','X'), 10, Size(b_width, b_height), true);
    if (!video.isOpened())
        throw((boost::format("This video is not open= %1%") % argv[4]).str());
    std::cout << "Videname: " << argv[4] << std::endl;

    //This rows only for debug
    //namedWindow("out", WINDOW_NORMAL);
    //moveWindow("out", 2000, 2000);
    //end
    Mat temp;
    double i = 0; 
    for(;w1 < max_w1 || h1 < max_h1 || w2 > max_w2 || h1 > max_h1;
            w1 -= 0.5, h1 -= 2, w2 += 0.5, h2 += 2) {
        if (i != 4) {
            i++;
            cout<< "Cnt if " << i << endl;
            continue;
        } else {
            i = 0;
            cout<< "Cnt cyc " << i << endl;
        }
        //cout<< "Cnt cyc " << i << endl;
        Rect r1(w1, h1, p1_width, p1_height);
        Rect r2(w2, h2, p2_width, p2_height);
        temp = back.clone();
        if (h2 < h1) {
            plate2.copyTo(temp(r2));
        } else {
            plate1.copyTo(temp(r1));
        }
        //imshow("out", temp);
        //cvWaitKey(5);
        cout<< "W1 = " << w1 << endl;
        cout<< "H1 = " << h1 << endl;
        cout<< "W2 = " << w2 << endl;
        cout<< "H2 = " << h2 << endl;
        video.write(temp);
        temp.release();
    }
    back.release();
    plate1.release();
    plate2.release();
    video.release();
    cout<<"End"<<endl;
    //destroyAllWindows();
    return 0;
}


int main(int argc, char* argv[]) {
    try {
        return my_main(argc, argv);
    } catch (std::string reason) {
        std::cerr << "Exc= "<< reason << std::endl;
        back.release();
        plate1.release();
        plate2.release();
        video.release();
        return 1;
    }
}