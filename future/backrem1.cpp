#include <string>
#include <forward_list>
#include "opencv2/opencv.hpp"

#include <boost/format.hpp>
#include <boost/thread.hpp>
#include <iostream>

class VWrite {
private:
    bool delfond = false;
    double cframe = 0.0f;
    cv::Mat background_mask;
    cv::VideoCapture video;
    cv::Ptr<BackgroundSubtractor> mog;
    cv::Size small_size(100, 100), norm_size;
 
    void fdelfond() {
        this->mog->apply(this->frame, this->background_mask);
        cv::Mat mask = this->background_mask.clone();
        cv::threshold(mask, mask, 128, 255, cv::CV_THRESH_BINARY);
        cv::resize(mask, mask, small_size);
        cv::medianBlur(mask, mask, 9);
        cv::erode(mask, mask, Mat());
        cv::dilate(mask, mask, Mat());
        cv::resize(mask, mask, this->norm_size);
        frame.copyTo(frame, mask);
        mask.release();
    }
 
public:
    VWrite(const char* videoname) {
		video.open(videoname);
        if(!video.isOpened()) {
            std::cerr <<"This video is not open: "<<videoname<< std::endl;
            delete this;
            throw ("Video is not open");
        }
        this->cframe = video.get(cv::CV_CAP_PROP_FRAME_COUNT);
    }
 
    void setfond(int history, double varThreshold, bool detectShadows) {
		this->delfond = true;
		this->mog = cv::createBackgroundSubtractorMOG2(history, varThreshold,
		    detectShadows);
    }
  
    void write(forward_list<double> &lframe, string &filejpg) {
	    vector<int> params = {cv::CV_IMWRITE_JPEG_QUALITY, 100};
        bool rframe, rwrite;
        double i = 0;
        Mat frame;
        for(double &j: lframe) {
			for(;i < j; i++) this->video.read(frame);
		    rframe = this->video.read(frame);
		    i++;
		    if (!rframe) {
                std::cerr << "Failed create frame in: " << j << std::endl;
                break;
            }
            if (this->delfond) this->fdelfond(frame);
            rwrite = cv::imwrite((boost::format("%1%_N%2%.jpg") % filejpg % j).str(), frame, params);
            if (!rwrite) {
                std::cerr << "Failed save the image in: " << j << std::endl;
            }
        }
        frame.release;
    }

    ~VWrite() {
		this->video.release();
		this->background_mask.release();
	}	
};
/*
void dbackground() {
		Mat frame, diff, temp, out;
        Size small_size(100, 100), norm_size;
        int history = 500;
        double varThreshold = 2;
        bool detectShadows = false;
        Ptr<BackgroundSubtractor> mog = createBackgroundSubtractorMOG2(history,
                                            varThreshold, detectShadows);
        namedWindow("Frame", WINDOW_NORMAL);
        moveWindow("Frame", 100, 100);
        namedWindow("Diff", WINDOW_NORMAL);
        moveWindow("Diff", 100, 100);
        namedWindow("Out", WINDOW_NORMAL);
        moveWindow("Out", 100, 100);
	    //Working program
        bool rframe, rwrite;
        for(double i = 0;;) {
		    this->video >> frame;
            if(frame.empty()) {
                cout << "Failed create frame " << endl;
                break;
            }
            mog->apply(frame, diff);
            //cvtColor(frame, frame, CV_BGR2GRAY);
            if (i == 0) {
		    	norm_size = frame.size();
		    	i++;
		    	continue;
		    }
            threshold(diff, diff, 128, 255, CV_THRESH_BINARY);
            temp = diff.clone();
            resize(temp, temp, small_size);
            medianBlur(temp, temp, 9);
            erode(temp, temp, Mat());
            dilate(temp, temp, Mat());
            resize(temp, temp, norm_size);
            out.release();
            frame.copyTo(out, temp);
            imshow("Frame", frame);
            imshow("Diff", diff);
            imshow("Out", out);
            waitKey(2);
        }
        temp.release();
        frame.release();
        diff.release();
        out.release();
        destroyAllWindows();
    }
*/
int main(int argc, char* argv[])
{
    VWrite video(argv[1]);
    video.dbackground();
    return 1;
}
